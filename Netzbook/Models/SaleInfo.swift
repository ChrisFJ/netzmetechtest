//
//  SaleInfo.swift
//  Netzbook
//
//  Created by AIA-Chris on 12/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import Foundation
// MARK: - SaleInfo
class SaleInfo: Codable {
    let buyLink: String?
    
    init(buyLink: String?) {
        self.buyLink = buyLink
    }
}
