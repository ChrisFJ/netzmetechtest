//
//  Item.swift
//  Netzbook
//
//  Created by AIA-Chris on 11/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import Mapper

class Item: Codable {
    let volumeInfo: VolumeInfo?
    let saleInfo: SaleInfo?

    init(volumeInfo:VolumeInfo, saleInfo: SaleInfo?) {
        self.volumeInfo = volumeInfo
        self.saleInfo = saleInfo
    }
}
