//
//  NetzDetailViewController.swift
//  Netzbook
//
//  Created by AIA-Chris on 13/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit
import Cosmos
import Kingfisher
import UIImageColors
import RxCocoa
import RxSwift

class NetzDetailViewController: UIViewController {

    @IBOutlet weak var labelBookTitle:UILabel!
    @IBOutlet weak var labelBookAuthors:UILabel!
    @IBOutlet weak var ratingView:CosmosView!
    @IBOutlet weak var imageViewCover:UIImageView!
    @IBOutlet weak var backIcon:UIButton!
    @IBOutlet weak var buttonPreview:UIButton!
    @IBOutlet weak var buttonBuy:UIButton!

    var book: Item?
    var authors = "by "
    var numberOfAuthors = 0
    var setting = CosmosSettings()
    var disposeBag:DisposeBag! = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupRatingView()
        setupImageCover()
        setupButtonPreview()
        bind()
    }
    
    func setupButtonPreview() {
        buttonPreview.layer.cornerRadius = buttonPreview.frame.height / 2
        buttonBuy.layer.cornerRadius = buttonBuy.frame.height / 2

        buttonPreview.rx.tap.subscribe { (_) in
            guard let url = URL(string: self.book?.volumeInfo?.previewLink ?? "") else { return }
            UIApplication.shared.open(url)
        }.disposed(by: disposeBag)
        
        buttonBuy.rx.tap.subscribe { (_) in
            guard let url = URL(string: self.book?.saleInfo?.buyLink ?? "") else { return }
            UIApplication.shared.open(url)
        }.disposed(by: disposeBag)
    }
    
    func setupRatingView() {
        setting.filledColor = UIColor(rgb: 0xFFCC00)
        setting.filledBorderColor = UIColor(rgb: 0xFFCC00)
        setting.emptyColor = UIColor(rgb: 0xD1DDDF)
        setting.emptyBorderColor = UIColor(rgb: 0xD1DDDF)
    }
    func setupImageCover() {
        imageViewCover.layer.cornerRadius = 4.0
        imageViewCover.kf.indicatorType = .activity
        guard let coverUrl = URL.init(string: book?.volumeInfo?.imageLinks?.thumbnail ?? "") else { return }
        imageViewCover.kf.setImage(with: coverUrl, placeholder: nil, options: nil, progressBlock: { (a, b) in
            
        }) { (image, error, cacheType, url) in
            let colors = image?.getColors()
            self.view.backgroundColor = colors?.background
            self.labelBookTitle.textColor = colors?.primary
            self.labelBookAuthors.textColor = colors?.secondary
            let origImage = self.backIcon.currentImage
            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
            self.backIcon.setImage(tintedImage, for: .normal)
            self.backIcon.tintColor = colors?.secondary
            self.ratingView.rating = self.book?.volumeInfo?.averageRating ?? 0.0
            self.setting.filledColor = (colors?.primary!)!
            self.setting.filledBorderColor = (colors?.primary!)!
        }
    }
    func bind() {
        labelBookTitle.text = book?.volumeInfo?.title
        for i in book?.volumeInfo?.authors ?? [String]() {
            if numberOfAuthors > 0 {
                authors.append(", \(i)")
                labelBookAuthors.text = authors
                numberOfAuthors += 1
            } else {
                authors.append(i)
                labelBookAuthors.text = authors
                numberOfAuthors += 1
            }
        }
    }
    
    @IBAction func didTapBack() {
        self.dismiss(animated: true, completion: nil)
    }
}
