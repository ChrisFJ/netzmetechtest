//
//  NetzBaseViewController.swift
//  Netzbook
//
//  Created by AIA-Chris on 12/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit

class NetzBaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @objc func adjustForKeyboard(notification: Notification) {
        
    }
    
}
