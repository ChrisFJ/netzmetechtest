//
//  ResGoogleBooks.swift
//  Netzbook
//
//  Created by AIA-Chris on 11/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit
import Mapper

// MARK: - ResGoogleBooks
class ResGoogleBooks: Codable {
    let kind: String
    let totalItems: Int
    let items: [Item]
    
//    required init(map: Mapper) throws {
//        try kind = map.from("kind")
//        try totalItems = map.from("totalItems")
//        items = map.optionalFrom("items")
//    }
    init(kind: String, totalItems: Int, items: [Item]) {
        self.kind = kind
        self.totalItems = totalItems
        self.items = items
    }
}
