//
//  VolumeInfo.swift
//  Netzbook
//
//  Created by AIA-Chris on 11/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import Mapper

// MARK: - VolumeInfo
class VolumeInfo: Codable {
    let title: String?
    let imageLinks: ImageLinks?
    let authors: [String]?
    let averageRating: Double?
    let publishedDate: String?
    let publisher: String?
    let previewLink: String?
    
    init(title:String?, imageLinks:ImageLinks?, authors:[String]?, averageRating:Double?, publishedDate:String?, publisher: String?, previewLink: String?) {
        self.publisher = publisher
        self.title = title
        self.imageLinks = imageLinks
        self.authors = authors
        self.publishedDate = publishedDate
        self.averageRating = averageRating
        self.previewLink = previewLink
    }
}
