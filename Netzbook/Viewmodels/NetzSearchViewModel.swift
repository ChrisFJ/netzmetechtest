//
//  NetzSearchViewModel.swift
//  Netzbook
//
//  Created by AIA-Chris on 11/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Moya
import Moya_ModelMapper

class NetzSearchViewModel: NSObject, UICollectionViewDataSource {
    
    var resGoogleBooks:ResGoogleBooks?
    var rxItems:Variable<[Item]>! = Variable([])
    var disposeBag:DisposeBag! = DisposeBag()
    var provider:MoyaProvider<GoogleBooksAPI>! = MoyaProvider<GoogleBooksAPI>(plugins: [CompleteUrlLoggerPlugin()])
    var keyword = ""
    
    func loadSearching(_ keyword: String, completion:@escaping (_ success:Bool?,_ error:String?) -> Void) {
        self.keyword = keyword
        provider.rx.request(.getSearchVolume(keyword: keyword))
                .filterSuccessfulStatusCodes()
                .mapJSON()
                .subscribe(onSuccess: { (response) in
                    guard let jsonData = try? JSONSerialization.data(withJSONObject:response) else { return }
                    do {
                        let decoder = JSONDecoder()
                        let res = try decoder.decode(ResGoogleBooks.self, from: jsonData)
                        self.resGoogleBooks = res
                        res.items.forEach({self.rxItems.value.append($0)})
                        DispatchQueue.main.async {
                            completion(true, nil)
                        }
                    } catch {
                        self.rxItems.value.removeAll()
                        print(error.localizedDescription)
                        completion(false, error.localizedDescription)
                    }
                }, onError: { (error) in
                    print(error.localizedDescription)
                    completion(false, error.localizedDescription)
                }).disposed(by: disposeBag)
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (self.rxItems.value.count == 0) {
            collectionView.setEmptyMessage("Nothing to show :(")
        } else {
            collectionView.restore()
        }
        return self.rxItems.value.count    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "booksCell", for: indexPath) as! NetzCVCellBook
        cell.bind(book: (rxItems.value[indexPath.row]))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerBooks", for: indexPath) as! NetzSearchViewHeader
        headerView.labelResult.text = rxItems.value.count > 0 ? "Over \(resGoogleBooks?.totalItems ?? 0) results for “\(keyword)”" : ""
        
        return headerView
    }
}


