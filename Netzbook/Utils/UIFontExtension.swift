//
//  UIFontExtension.swift
//  Netzbook
//
//  Created by AIA-Chris on 13/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit
extension UIFont {
    public class func PoppinsRegular(with size:CGFloat) -> UIFont {
        return UIFont(name: "Poppins-Regular", size: size)!
    }
    public class func PoppinsBold(with size:CGFloat) -> UIFont {
        return UIFont(name: "Poppins-Bold", size: size)!
    }
    public class func PoppinsMedium(with size:CGFloat) -> UIFont {
        return UIFont(name: "Poppins-Medium", size: size)!
    }
}
