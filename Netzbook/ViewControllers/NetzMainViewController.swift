//
//  ViewController.swift
//  Netzbook
//
//  Created by AIA-Chris on 11/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class NetzMainViewController: NetzBaseViewController, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {

    @IBOutlet weak var collectionBooks:UICollectionView!
    let searchBar:UISearchBar = UISearchBar(frame: .zero)
    
    fileprivate let viewModel = NetzSearchViewModel()
    fileprivate let activityView = UIActivityIndicatorView(style: .whiteLarge)
    let fadeView:UIView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        setupSearchBar()
    }
    
    func setupCollectionView() {
        collectionBooks.register(UINib(nibName: "NetzCVCellBook", bundle: nil), forCellWithReuseIdentifier: "booksCell")
        collectionBooks.dataSource = viewModel
        collectionBooks.delegate = self
        
        collectionBooks.rx.itemSelected.subscribe { (indexPath) in
            let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "detailVC") as! NetzDetailViewController
            let index = indexPath.element?.row
            detailVC.book = self.viewModel.rxItems.value[index!]
            self.present(detailVC, animated: true, completion: nil)
        }.disposed(by: self.viewModel.disposeBag)
    }
    
    func setupSearchBar() {
        self.navigationItem.titleView = self.searchBar
        searchBar.searchBarStyle = .minimal
        searchBar.barStyle = .blackOpaque
        searchBar.placeholder = "Search Books"
        //Change TextColor
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.font = UIFont.PoppinsMedium(with: 17)
        textFieldInsideSearchBar?.textColor = UIColor(rgb: 0x242126)
        //Chage PlaceholderColor
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.font = UIFont.PoppinsMedium(with: 17)
        textFieldInsideSearchBarLabel?.textColor = UIColor(rgb: 0xAFC1C4)
        // Glass Icon Customization
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = UIColor(rgb: 0x242126)
        self.searchBar.isHidden = false
        
        //subscribe for searchbar text changes
        searchBar.rx.text
            .orEmpty
            .debounce(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
        .subscribe({ [unowned self] query in
            self.viewModel.rxItems.value.removeAll()
            if query.element?.count ?? 0 > 0 {
                self.searchBar.showsCancelButton = true
                self.showIndicatorLoading()
                DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                    self.viewModel.loadSearching(query.element!, completion: { (success, error) in
                        self.reload()
                    })
                }
            } else {
                self.reload()
            }
        }).disposed(by: viewModel.disposeBag)
        
        //subscribe for cancel key tapped
        searchBar.rx.cancelButtonClicked.subscribe { (_) in
            self.searchBar.showsCancelButton = false
            self.searchBar.text = ""
            self.searchBar.resignFirstResponder()
            self.view.endEditing(true)
        }.disposed(by: self.viewModel.disposeBag)
        
        //subscribe for search key tapped
        searchBar.rx.searchButtonClicked.subscribe { (_) in
            self.searchBar.resignFirstResponder()
        }.disposed(by: self.viewModel.disposeBag)
    }
    
    private func showIndicatorLoading() {
        fadeView.frame = self.view.frame
        fadeView.backgroundColor = UIColor.white
        fadeView.alpha = 0.4
        self.view.addSubview(fadeView)
        self.view.addSubview(activityView)
        activityView.hidesWhenStopped = true
        activityView.center = self.view.center
        activityView.startAnimating()
    }
    
    override func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            collectionBooks.contentInset = .zero
        } else {
            collectionBooks.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }
        
        collectionBooks.scrollIndicatorInsets = collectionBooks.contentInset
    }
    
    private func reload() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                self.collectionBooks.reloadData()
                self.collectionBooks.alpha = 1
                self.fadeView.removeFromSuperview()
                self.activityView.stopAnimating()
            }, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collectionView.frame.width
        return CGSize(width: w, height: 200)
    }
    
    
}
