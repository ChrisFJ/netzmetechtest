//
//  ImageLinks.swift
//  Netzbook
//
//  Created by AIA-Chris on 11/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import Mapper

class ImageLinks: Codable {
    let smallThumbnail, thumbnail: String?
    
    init(smallThumbnail: String?, thumbnail: String?) {
        self.smallThumbnail = smallThumbnail
        self.thumbnail = thumbnail
    }
}
