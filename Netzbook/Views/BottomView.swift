//
//  BottomView.swift
//  Netzbook
//
//  Created by AIA-Chris on 13/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit

class BottomView: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
        roundCorners(corners: [.topLeft, .topRight], radius: 16)
    }

}
