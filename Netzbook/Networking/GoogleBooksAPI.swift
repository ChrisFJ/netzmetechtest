//
//  GoogleBooksAPI.swift
//  Netzbook
//
//  Created by AIA-Chris on 11/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import Foundation
import Moya

enum GoogleBooksAPI: TargetType {

    var headers: [String : String]? {return [:]}
    
    case getSearchVolume(keyword:String)
    
    var baseURL: URL { return URL(string: "https://www.googleapis.com/books/v1")! }
    
    var path: String {
        switch self {
        case .getSearchVolume:
            return "/volumes"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getSearchVolume:
            return .get
        }
    }
    
    var sampleData: Data { return Data() }
    
    var parameters: [String: Any]? {
        switch self {
        case .getSearchVolume(let keyword):
            return["q": keyword]
        }
    }
    
    var task: Task {
        switch self {
        case .getSearchVolume:
            return .requestParameters(parameters: parameters!, encoding: URLEncoding.queryString)
        }
    }
    
    
}
