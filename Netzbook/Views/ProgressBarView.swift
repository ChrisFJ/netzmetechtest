//
//  ProgressBarView.swift
//  Netzbook
//
//  Created by AIA-Chris on 11/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit

class ProgressBarView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [.repeat, .autoreverse], animations: {
            self.alpha = 0.1
        }, completion: nil)
    }

}
