//
//  NetzCVCellBook.swift
//  Netzbook
//
//  Created by AIA-Chris on 11/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit
import Kingfisher
import Cosmos

class NetzCVCellBook: UICollectionViewCell {

    @IBOutlet weak var imageViewCover:UIImageView!
    @IBOutlet weak var labelBookTitle:UILabel!
    @IBOutlet weak var labelBookAuthors:UILabel!
    @IBOutlet weak var ratingView:CosmosView!
    @IBOutlet weak var labelPublishDate:UILabel!
    @IBOutlet weak var labelPublisher:UILabel!
    
    var authors = "by "
    var numberOfAuthors = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageViewCover.layer.cornerRadius = 4
        imageViewCover.layer.borderColor = UIColor.black.withAlphaComponent(0.1).cgColor
        imageViewCover.layer.borderWidth = 0.5
        imageViewCover.layer.shadowColor = UIColor.black.cgColor
        imageViewCover.layer.shadowOffset = CGSize(width: 0, height: 2)
    }

    func bind(book:Item) {
        labelPublisher.text = book.volumeInfo?.publisher
        labelPublishDate.text = book.volumeInfo?.publishedDate
        ratingView.rating = book.volumeInfo?.averageRating ?? 0.0
        labelBookTitle.text = book.volumeInfo?.title
        imageViewCover.kf.indicatorType = .activity
        imageViewCover.kf.setImage(with: URL.init(string: book.volumeInfo?.imageLinks?.thumbnail ?? ""))
        for i in book.volumeInfo?.authors ?? [String]() {
            if numberOfAuthors > 0 {
                authors.append(", \(i)")
                labelBookAuthors.text = authors
                numberOfAuthors += 1
            } else {
                authors.append(i)
                labelBookAuthors.text = authors
                numberOfAuthors += 1
            }
        }
    }
}
